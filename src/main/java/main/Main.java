package main;

import java.time.LocalDate;
import java.time.Month;
import java.util.HashSet;
import java.util.Set;
import controlador.Controlador;
import dao.Dao;
import excepcion.BadPostCodeException;
import modelo.Alquiler;
import modelo.ArancelEspecial;
import modelo.Casa;
import modelo.Cliente;
import modelo.ComicionInmobiliaria;
import modelo.Garante;
import modelo.Locador;
import modelo.Locatario;
import modelo.Pago;
import modelo.Recargo;
import vistas.viewPrincipal;

public class Main {

	public static void main(String[] args) {
		Dao dao = new Dao();
		Controlador controlador = new Controlador();
		Pago pago;
		Locador locador = null;
		Locatario locatario = null;
		Garante garante = null;
		Cliente cliente;
		try {
			cliente = new Cliente("DNI", "33222111", "Juan", "Quiros", "Soltero", "Corrientes",
					"3758-405436", "juanquiros@gmail.com");
			dao.Guardar(cliente);
			garante = new Garante(cliente, "Estudiante", "Comprobante pago");
			dao.Guardar(garante);
			cliente = new Cliente("DNI", "44333222", "Tamara", "Prokopiw ", "Casada ", "libertad",
					"3758-405438", "tamara@gmail.com");
			dao.Guardar(cliente);
			locador = new Locador(cliente, "27443332229", "Tamara ");
			dao.Guardar(locador);
			
			cliente = new Cliente("DNI", "55444333", "Paula", "Quiros", "Soltera", "630",
					"3758-456590", "quiros@gmail.com");
			dao.Guardar(cliente);
			locatario = new Locatario(cliente, "Arquitecta", "Pago de haberes", "pauquiro", "Paupau01");
			dao.Guardar(locatario);
		} catch (BadPostCodeException e) {
			e.printStackTrace();
		}
		
		ComicionInmobiliaria comicionInmobiliaria = new ComicionInmobiliaria(3400, 3);
		Recargo recargo = new Recargo(450, 15, true);
		ArancelEspecial arancelEspecial = new ArancelEspecial("Luz", 345, true,null);
		
		
		
		
		Casa casa = new Casa("Hussay y Libertad", 60, false, locador);
		dao.Guardar(casa);
		Alquiler alquiler = new Alquiler(LocalDate.now(), 6, true, comicionInmobiliaria, recargo,
				casa, null, null, garante, locatario);
		
		dao.Guardar(alquiler);
		arancelEspecial.setContrato(alquiler);
		dao.Guardar(arancelEspecial);
		
		//(float monto, LocalDate primerVencimiento, LocalDate segundoVencimiento, boolean estadoDelPago, LocalDate fechaDelPago, Contrato contrato)
		pago = new Pago(500,LocalDate.of(2021, Month.JANUARY, 20) , LocalDate.of(2021, Month.JANUARY, 27), true, LocalDate.now(), alquiler);
		dao.Guardar(pago);
		pago = new Pago(600,LocalDate.of(2021, Month.JANUARY, 30) , LocalDate.of(2021, Month.FEBRUARY, 27), false, null, alquiler);
		dao.Guardar(pago);
		pago = new Pago(600,LocalDate.of(2021, Month.MARCH, 20) , LocalDate.of(2021, Month.MARCH, 27), false, null, alquiler);
		dao.Guardar(pago);
		pago = new Pago(700,LocalDate.of(2021, Month.MAY, 20) , LocalDate.of(2021, Month.MAY, 27), false, null, alquiler);
		dao.Guardar(pago);
		pago = new Pago(700,LocalDate.of(2021, Month.JUNE, 20) , LocalDate.of(2021, Month.JUNE, 27), false, null, alquiler);
		dao.Guardar(pago);
		pago = new Pago(800,LocalDate.of(2021, Month.JULY, 20) , LocalDate.of(2021, Month.JULY, 27), false, null, alquiler);
		dao.Guardar(pago);
		viewPrincipal ventanaPrincipal = new viewPrincipal(controlador);
		ventanaPrincipal.setVisible(true);

	}

}
