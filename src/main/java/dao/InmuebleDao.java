package dao;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.Transaction;

import modelo.Casa;
import modelo.Departamento;
import modelo.Inmueble;
import modelo.LocalComercial;
import modelo.Locador;
import modelo.Terreno;
import util.HibernateUtil;

public class InmuebleDao {
	

	public List<Terreno> getTerrenoPorM2(int min, int max) {   
		List<Terreno> terreno = null; 
        Session sesion= HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = sesion.beginTransaction();
        CriteriaBuilder cb = sesion.getCriteriaBuilder();
        CriteriaQuery<Terreno> cq = cb.createQuery(Terreno.class);
        CriteriaQuery<Terreno> all = null;
        Root<Terreno> rootEntry = cq.from(Terreno.class);
        all = cq.select(rootEntry).where(cb.between(rootEntry.get("metrosCuadrados"), min,max));
        TypedQuery<Terreno> allQuery = sesion.createQuery(all);        
        if(allQuery.getResultList()!=null && !allQuery.getResultList().isEmpty()){
        	terreno = (List<Terreno>) allQuery.getResultList();
            }
        tx.commit();
        return terreno;       
    }
	public List<Casa> getCasaPorM2(int min, int max) {   
		List<Casa> casa = null; 
        Session sesion= HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = sesion.beginTransaction();
        CriteriaBuilder cb = sesion.getCriteriaBuilder();
        CriteriaQuery<Casa> cq = cb.createQuery(Casa.class);
        CriteriaQuery<Casa> all = null;
        Root<Casa> rootEntry = cq.from(Casa.class);
        all = cq.select(rootEntry).where(cb.between(rootEntry.get("metrosCuadrados"), min,max));
        TypedQuery<Casa> allQuery = sesion.createQuery(all);        
        if(allQuery.getResultList()!=null && !allQuery.getResultList().isEmpty()){
        	casa = (List<Casa>) allQuery.getResultList();
            }
        tx.commit();
        return casa;       
    }
	public List<Departamento> getDepartamentoPorM2(int min, int max) {   
		List<Departamento> departamento = null; 
        Session sesion= HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = sesion.beginTransaction();
        CriteriaBuilder cb = sesion.getCriteriaBuilder();
        CriteriaQuery<Departamento> cq = cb.createQuery(Departamento.class);
        CriteriaQuery<Departamento> all = null;
        Root<Departamento> rootEntry = cq.from(Departamento.class);
        all = cq.select(rootEntry).where(cb.between(rootEntry.get("metrosCuadrados"), min,max));
        TypedQuery<Departamento> allQuery = sesion.createQuery(all);        
        if(allQuery.getResultList()!=null && !allQuery.getResultList().isEmpty()){
        	departamento = (List<Departamento>) allQuery.getResultList();
            }
        tx.commit();
        return departamento;       
    }
	public List<LocalComercial> getLocalComercialPorM2(int min, int max) {   
		List<LocalComercial> localComercial = null; 
        Session sesion= HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = sesion.beginTransaction();
        CriteriaBuilder cb = sesion.getCriteriaBuilder();
        CriteriaQuery<LocalComercial> cq = cb.createQuery(LocalComercial.class);
        CriteriaQuery<LocalComercial> all = null;
        Root<LocalComercial> rootEntry = cq.from(LocalComercial.class);
        all = cq.select(rootEntry).where(cb.between(rootEntry.get("metrosCuadrados"), min,max));
        TypedQuery<LocalComercial> allQuery = sesion.createQuery(all);        
        if(allQuery.getResultList()!=null && !allQuery.getResultList().isEmpty()){
        	localComercial = (List<LocalComercial>) allQuery.getResultList();
            }
        tx.commit();
        return localComercial;       
    }
	public List<Casa> getCasas() {
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			return session.createQuery("from Casa", Casa.class).list();
		}
	}
	public List<Departamento> getDepartamentos() {
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			return session.createQuery("from Departamento", Departamento.class).list();
		}
	}
	public List<LocalComercial> getLocalesComerciales() {
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			return session.createQuery("from LocalComercial", LocalComercial.class).list();
		}
	}
	public List<Terreno> getTerrenos() {
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			return session.createQuery("from Terreno", Terreno.class).list();
		}
	}
	public List<Inmueble> getInmueblePorLocador(Locador locador) {
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			return session.createQuery("from Inmueble where locador = :locador", Inmueble.class).setParameter("locador", locador).list();
		}
	}
}
