package dao;

import java.util.List;
import javax.persistence.TypedQuery;
import org.hibernate.Session;
import modelo.Comprador;
import modelo.Venta;
import util.HibernateUtil;

public class VentaDao {
	

		public List<Venta> getVentaPorComprador(Comprador comprador) {   
			try (Session session = HibernateUtil.getSessionFactory().openSession()) {						
				 TypedQuery<Venta> allQuery = session.createQuery("from Venta where comprador = :comprador", Venta.class).setParameter("comprador",comprador );      
				 if(allQuery.getResultList()!=null && !allQuery.getResultList().isEmpty()){
			        	return  allQuery.getResultList();
			            }else{
			            	return null;
			            }
			}     
	    }
		public List<Venta> getVentas() {
			try (Session session = HibernateUtil.getSessionFactory().openSession()) {
				return session.createQuery("from Venta", Venta.class).list();
			}
		}
	

}
