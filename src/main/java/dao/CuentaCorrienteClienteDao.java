package dao;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.Transaction;

import modelo.ArancelEspecial;
import modelo.Contrato;
import modelo.Pago;
import util.HibernateUtil;

public class CuentaCorrienteClienteDao {


public List<Pago> getPagos_Contrato(Contrato contrato){
	try (Session session = HibernateUtil.getSessionFactory().openSession()) {						
		 TypedQuery<Pago> allQuery = session.createQuery("from Pago where contrato = :cod_contrato", Pago.class).setParameter("cod_contrato",contrato );      
		 if(allQuery.getResultList()!=null && !allQuery.getResultList().isEmpty()){
	        	return  allQuery.getResultList();
	            }else{
	            	return null;
	            }
	}
}
public List<ArancelEspecial> getAranceles_Contrato(Contrato contrato){
	try (Session session = HibernateUtil.getSessionFactory().openSession()) {						
		 TypedQuery<ArancelEspecial> allQuery = session.createQuery("from ArancelEspecial where contrato = :cod_contrato", ArancelEspecial.class).setParameter("cod_contrato",contrato );      
		 if(allQuery.getResultList()!=null && !allQuery.getResultList().isEmpty()){
	        	return  allQuery.getResultList();
	            }else{
	            	return null;
	            }
	}
}

public String getEstadoPagos_Contrato(Contrato contrato){
	Long impagas = (long) 0;
	String estado="pagado";
	try (Session session = HibernateUtil.getSessionFactory().openSession()) {						
		 TypedQuery<Long> allQuery = session.createQuery("Select count(*)from Pago where contrato = :cod_contrato and primerVencimiento<= CURDATE() and fechaDelPago is null", Long.class).setParameter("cod_contrato",contrato );      
		 if(allQuery.getResultList()!=null && !allQuery.getResultList().isEmpty()){
			 	impagas =  allQuery.getResultList().get(0);
			 	estado =	"Debe " + impagas;
			 	if(impagas > 1){estado += " cuotas";}else{estado += " cuota";}	           
	            }
     	return estado;
	}
}
}
