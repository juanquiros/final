package dao;

import java.util.List;
import javax.persistence.TypedQuery;

import org.hibernate.Session;
import modelo.Alquiler;
import modelo.Garante;
import modelo.Locatario;
import util.HibernateUtil;

public class AlquilerDao {
	public List<Alquiler> getAlquilerPorLocatario(Locatario locatario) {   
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {						
			 TypedQuery<Alquiler> allQuery = session.createQuery("from Alquiler where locatario = :locatario", Alquiler.class).setParameter("locatario",locatario );      
			 if(allQuery.getResultList()!=null && !allQuery.getResultList().isEmpty()){
		        	return  allQuery.getResultList();
		            }else{
		            	return null;
		            }
		}     
    }
	public List<Alquiler> getAlquileres() {
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			return session.createQuery("from Alquiler", Alquiler.class).list();
		}
	}
	
	public boolean garanteRegistradoEnContrato(Garante garante) {
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			if(session.createQuery("from Alquiler where garante =:garante", Alquiler.class).setParameter("garante",garante).list().size() > 0) {
				return true;
			}else {
				return false;
			}
			
		}
	}
}
