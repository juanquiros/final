/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;



import java.time.LocalDate;
import java.time.ZoneId;

import java.util.*;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;




import controlador.Controlador;
import excepcion.BadPostCodeException;
import modelo.Alquiler;
import modelo.ArancelEspecial;
import modelo.Contrato;
import modelo.Pago;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout;
import java.awt.Font;
import java.awt.Color;
import javax.swing.UIManager;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 *
 * @author Juan Quiros
 */
public class viewPagar  extends javax.swing.JDialog  {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
     * Creates new form viewPagar
     */
	 Controlador controlador = null;
	 Contrato contrato = null;
	 Pago pago = null;
	 double total = 0;
	 double aranceles= 0;
	 double recargo=0;
	 double comicion=0;
	 int dias_recargo = 0;
	 int cuota= 1;
	 
	 
    public viewPagar(java.awt.Frame parent, boolean modal, Controlador controlador_, Pago pago_, int cuota,Contrato contrato_)  throws BadPostCodeException {
    	super(parent, modal);
    	setLocationRelativeTo(parent);
        initComponents();
        this.lblFecha.setText(LocalDate.now().toString());
        
    	if(controlador_ == null)
			throw new BadPostCodeException("Error, reintente");
    	if(contrato_ == null) 
    		throw new BadPostCodeException("Error, contrato no v�lido"); 
    	if(pago_==null)
    		throw new BadPostCodeException("Error, cuota incorrecta");
    	if (pago_.getFechaDelPago() != null)
    		throw new BadPostCodeException("Error, la cuota ya esta pagada");
    	this.controlador = controlador_;  
    	this.cuota = cuota;
    	this.pago = pago_;
    	this.contrato = contrato_;
    	
    
    	
    	cargar_pago();
    	
    	
    }

	private void cargar_pago() {

		this.txtCuota.setText("" + cuota);
		this.txtMonto.setText("$" + this.pago.getMonto());
		this.txtComicion.setText("0");

		this.txtPrimerVen.setText(this.pago.getPrimerVencimiento().toString());
		this.txtSegundoVen.setText(this.pago.getSegundoVencimiento().toString());

		if (this.contrato.getComicionInmobiliari().getCantidadDePagos() >= cuota) {
			this.txtComicion.setText("$" + this.contrato.getComicionInmobiliari().getMontoComicion());
			this.comicion = this.contrato.getComicionInmobiliari().getMontoComicion();
		}

		if (this.pago.getPrimerVencimiento().compareTo(LocalDate.now()) < 0) {
			this.txtEstadoCuota.setText("Impago Vencida");
			this.txtRecargo.setText(this.contrato.getUnRecargo().getMontoFijo() + "");
			this.recargo = this.contrato.getUnRecargo().getMontoFijo();
		} else {
			this.txtEstadoCuota.setText("Impago");
		}

		if (this.pago.getSegundoVencimiento().compareTo(LocalDate.now()) < 0) {

			// contar dias
			ZoneId defaultZoneId = ZoneId.systemDefault();
			Date fecha_vence = Date.from(this.pago.getSegundoVencimiento().atStartOfDay(defaultZoneId).toInstant());
			Date now = Date.from(LocalDate.now().atStartOfDay(defaultZoneId).toInstant());
			this.dias_recargo = this.getDifferenceDays(fecha_vence, now);
			// calcular recargo
			this.recargo += this.dias_recargo * this.contrato.getUnRecargo().getMontoProporcional();
			this.txtRecargo.setText("$" + this.recargo);

		}

		ArrayList<ArancelEspecial> aranceles = this.controlador.getArancelesContrato(this.contrato);
		if (aranceles != null) {
			for (ArancelEspecial arancel : aranceles) {
				this.aranceles += arancel.getMonto();
			}
		}

		this.total = this.comicion + this.aranceles + this.recargo + this.pago.getMonto();

		this.txtArancel.setText("$" + this.aranceles);
		this.txtTotal.setText("$" + this.total);
	}

    public int getDifferenceDays(Date d1, Date d2) {
    	long diff = d2.getTime() - d1.getTime();
    	return (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    	}                   
    
    public void pagar_cuota() {
    	this.pago.setMonto((float)this.total);
    	this.pago.setEstadoDelPago(true);
    	this.pago.setFechaDelPago(LocalDate.now());
    	this.controlador.actualizarPago(this.pago);
    	if(this.contrato.getDuracion()== this.cuota && this.contrato instanceof Alquiler) {
    		if(((Alquiler)this.contrato).getCasaAlquilada()!=null) { 
    			((Alquiler)this.contrato).getCasaAlquilada().setHabilidato(true); 
    			this.controlador.actualizarInmueble(((Alquiler)this.contrato).getCasaAlquilada());}
    		if(((Alquiler)this.contrato).getDepartamentoAlquilado()!=null) { 
    			((Alquiler)this.contrato).getDepartamentoAlquilado().setHabilidato(true);
    			this.controlador.actualizarInmueble(((Alquiler)this.contrato).getDepartamentoAlquilado());}    		
    		if(((Alquiler)this.contrato).getLocalComercialAlquilado()!=null) { 
    			((Alquiler)this.contrato).getLocalComercialAlquilado().setHabilidato(true);
    			this.controlador.actualizarInmueble(((Alquiler)this.contrato).getLocalComercialAlquilado());}
    		
    		
    	}
    	this.dispose();
    }
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtCuota = new javax.swing.JTextField();
        txtCuota.setEditable(false);
        txtMonto = new javax.swing.JTextField();
        txtMonto.setHorizontalAlignment(SwingConstants.RIGHT);
        txtMonto.setEditable(false);
        txtMonto.setForeground(Color.RED);
        txtMonto.setFont(new Font("Tahoma", Font.BOLD, 11));
        txtPrimerVen = new javax.swing.JTextField();
        txtPrimerVen.setEditable(false);
        txtSegundoVen = new javax.swing.JTextField();
        txtSegundoVen.setEditable(false);
        txtEstadoCuota = new javax.swing.JTextField();
        txtEstadoCuota.setEditable(false);
        jLabel7 = new javax.swing.JLabel();
        txtArancel = new javax.swing.JTextField();
        txtArancel.setHorizontalAlignment(SwingConstants.RIGHT);
        txtArancel.setEditable(false);
        txtArancel.setForeground(Color.RED);
        txtArancel.setFont(new Font("Tahoma", Font.BOLD, 11));
        jLabel8 = new javax.swing.JLabel();
        txtRecargo = new javax.swing.JTextField();
        txtRecargo.setHorizontalAlignment(SwingConstants.RIGHT);
        txtRecargo.setEditable(false);
        txtRecargo.setForeground(Color.RED);
        txtRecargo.setFont(new Font("Tahoma", Font.BOLD, 11));
        jLabel9 = new javax.swing.JLabel();
        txtTotal = new javax.swing.JTextField();
        txtTotal.setHorizontalAlignment(SwingConstants.RIGHT);
        txtTotal.setEditable(false);
        btnPagar = new javax.swing.JButton();
        btnPagar.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mousePressed(MouseEvent arg0) {
        		
        		pagar_cuota();
        	}
        });
        btnCancelar = new javax.swing.JButton();
        btnCancelar.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mousePressed(MouseEvent e) {
        		dispose();
        	}
        });
        lblFecha = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtComicion = new javax.swing.JTextField();
        txtComicion.setHorizontalAlignment(SwingConstants.RIGHT);
        txtComicion.setEditable(false);
        txtComicion.setForeground(Color.RED);
        txtComicion.setFont(new Font("Tahoma", Font.BOLD, 11));

        

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jLabel1.setText("Pagar");

        jLabel2.setText("Cuota ");

        jLabel3.setText("Monto");

        jLabel4.setText("Primer vencimiento");

        jLabel5.setText("Segundo vencimiento");

        jLabel6.setText("Estado de la cuota");

        txtCuota.setText("1");

        jLabel7.setText("Aranceles");

        jLabel8.setText("Recargo");

        jLabel9.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jLabel9.setText("Monto Total");

        txtTotal.setFont(new java.awt.Font("Dialog", 1, 36)); // NOI18N

        btnPagar.setText("Pagar");

        btnCancelar.setText("Cancelar");

        lblFecha.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        lblFecha.setText("jLabel10");

        jLabel10.setText("Comici�n Inmobiliaria");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1Layout.setHorizontalGroup(
        	jPanel1Layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(jPanel1Layout.createSequentialGroup()
        			.addGap(22)
        			.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
        				.addComponent(jLabel2)
        				.addComponent(jLabel3)
        				.addComponent(jLabel4)
        				.addComponent(jLabel5)
        				.addComponent(jLabel7)
        				.addComponent(jLabel1)
        				.addGroup(jPanel1Layout.createSequentialGroup()
        					.addPreferredGap(ComponentPlacement.RELATED)
        					.addComponent(jLabel9))
        				.addComponent(jLabel10)
        				.addComponent(jLabel6)
        				.addComponent(jLabel8))
        			.addGap(27)
        			.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
        				.addGroup(jPanel1Layout.createSequentialGroup()
        					.addComponent(btnPagar)
        					.addPreferredGap(ComponentPlacement.RELATED)
        					.addComponent(btnCancelar))
        				.addComponent(lblFecha)
        				.addComponent(txtTotal, GroupLayout.PREFERRED_SIZE, 149, GroupLayout.PREFERRED_SIZE)
        				.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING, false)
        					.addComponent(txtRecargo, Alignment.LEADING)
        					.addComponent(txtEstadoCuota, Alignment.LEADING)
        					.addComponent(txtComicion, Alignment.LEADING)
        					.addComponent(txtCuota, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 149, Short.MAX_VALUE)
        					.addComponent(txtMonto, Alignment.LEADING)
        					.addComponent(txtPrimerVen, Alignment.LEADING)
        					.addComponent(txtSegundoVen, Alignment.LEADING)
        					.addComponent(txtArancel, Alignment.LEADING)))
        			.addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
        	jPanel1Layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(jPanel1Layout.createSequentialGroup()
        			.addContainerGap()
        			.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
        				.addComponent(jLabel1)
        				.addComponent(lblFecha))
        			.addPreferredGap(ComponentPlacement.UNRELATED)
        			.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
        				.addComponent(jLabel2)
        				.addComponent(txtCuota, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
        				.addComponent(jLabel3)
        				.addComponent(txtMonto, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
        				.addComponent(jLabel4)
        				.addComponent(txtPrimerVen, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
        				.addComponent(jLabel5)
        				.addComponent(txtSegundoVen, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
        				.addComponent(jLabel7)
        				.addComponent(txtArancel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        			.addPreferredGap(ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
        			.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
        				.addComponent(jLabel10)
        				.addComponent(txtComicion, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
        				.addComponent(jLabel6)
        				.addComponent(txtEstadoCuota, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
        				.addComponent(jLabel8)
        				.addComponent(txtRecargo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
        				.addComponent(jLabel9)
        				.addComponent(txtTotal, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        			.addGap(18)
        			.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
        				.addComponent(btnPagar)
        				.addComponent(btnCancelar))
        			.addGap(32))
        );
        jPanel1.setLayout(jPanel1Layout);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        layout.setHorizontalGroup(
        	layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(layout.createSequentialGroup()
        			.addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, 375, GroupLayout.PREFERRED_SIZE)
        			.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
        	layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(layout.createSequentialGroup()
        			.addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        			.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().setLayout(layout);

        pack();
    }// </editor-fold>                        

    // Variables declaration - do not modify                     
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnPagar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txtComicion;
    private javax.swing.JLabel lblFecha;
    private javax.swing.JTextField txtArancel;
    private javax.swing.JTextField txtCuota;
    private javax.swing.JTextField txtEstadoCuota;
    private javax.swing.JTextField txtMonto;
    private javax.swing.JTextField txtPrimerVen;
    private javax.swing.JTextField txtRecargo;
    private javax.swing.JTextField txtSegundoVen;
    private javax.swing.JTextField txtTotal;
    // End of variables declaration                   
}
