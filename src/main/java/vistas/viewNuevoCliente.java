/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;

import javax.swing.JOptionPane;

import controlador.Controlador;
import excepcion.BadPostCodeException;
import modelo.Cliente;
import modelo.Comprador;
import modelo.Garante;
import modelo.Locador;
import modelo.Locatario;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 *
 * @author Juan Quiros
 */
public class viewNuevoCliente extends javax.swing.JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Creates new form viewNuevoCliente
	 * 
	 * @param parent
	 * @param modal
	 */
	private java.awt.Checkbox checkComprador;
	private java.awt.Checkbox checkGarante;
	private java.awt.Checkbox checkLocador;
	private java.awt.Checkbox checkLocatario;
	private java.awt.Checkbox checkbox1;
	private java.awt.Checkbox checkbox3;
	private javax.swing.JButton btnGuardar;
	private javax.swing.JButton jButton2;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel10;
	private javax.swing.JLabel jLabel11;
	private javax.swing.JLabel jLabel12;
	private javax.swing.JLabel jLabel17;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JLabel jLabel5;
	private javax.swing.JLayeredPane jLayeredPane1;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JLabel lblActividadRealiza;
	private javax.swing.JLabel lblComprobante;
	private javax.swing.JLabel lblContrasenia;
	private javax.swing.JLabel lblCuit;
	private javax.swing.JLabel lblRazon;
	private javax.swing.JLabel lblUsuario;
	private java.awt.Panel panel1;
	private javax.swing.JTextField txtActividad;
	private javax.swing.JTextField txtApellido;
	private javax.swing.JTextField txtComprobante;
	private javax.swing.JTextField txtContrasenia;
	private javax.swing.JTextField txtCuit;
	private javax.swing.JTextField txtDocumento;
	private javax.swing.JTextField txtDomicilio;
	private javax.swing.JTextField txtEmail;
	private javax.swing.JTextField txtEstadoCivil;
	private javax.swing.JTextField txtNombre;
	private javax.swing.JTextField txtRazonSocial;
	private javax.swing.JTextField txtTelefono;
	private javax.swing.JTextField txtTipoDoc;
	private javax.swing.JTextField txtUsuario;

	private Cliente cliente;
	private Cliente NuevoCliente;
	private Controlador controlador;
	private Locador locador = null;
	private Locatario locatario = null;
	private Garante garante = null;
	private Comprador comprador = null;

	public viewNuevoCliente(java.awt.Frame parent, boolean modal, Controlador controlador, Cliente cliente) {
		super(parent, modal);
		setLocationRelativeTo(null);
		initComponents();
		this.controlador = controlador;
		if (cliente != null) {
			this.cliente = cliente;
			this.modoActualizarCliente();
		}
	}

	private void modoActualizarCliente() {
		this.btnGuardar.setText("Actualizar");
		this.locador = this.controlador.obtenerLocadorPorDocumento(this.cliente.getDocumento());
		this.locatario = this.controlador.obtenerLocatarioPorDocumento(this.cliente.getDocumento());
		this.comprador = this.controlador.obtenerCompradorPorDocumento(this.cliente.getDocumento());
		this.garante = this.controlador.obtenerGarantePorDocumento(this.cliente.getDocumento());

		if (this.locador != null) {
			this.txtCuit.setText(this.locador.getCuit());
			this.txtRazonSocial.setText(this.locador.getRazonSocial());
			this.checkLocador.setState(true);
			this.checkLocador.setEnabled(false);

		}
		if (this.comprador != null) {
			this.txtUsuario.setText(this.comprador.getUsuarioWeb());
			this.txtActividad.setText(this.comprador.getActivadaQueRealiza());
			this.txtComprobante.setText(this.comprador.getComprobanteIngresos());
			this.txtContrasenia.setText(this.comprador.getContraseniaWeb());
			this.checkComprador.setState(true);
			this.checkComprador.setEnabled(false);
		}
		if (this.locatario != null) {
			this.txtUsuario.setText(this.locatario.getUsuarioWeb());
			this.txtActividad.setText(this.locatario.getActividadQueRealiza());
			this.txtComprobante.setText(this.locatario.getComprobanteIngreso());
			this.txtContrasenia.setText(this.locatario.getContrasenia());
			this.checkLocatario.setState(true);
			this.checkLocatario.setEnabled(false);
		}
		if (this.garante != null) {
			this.txtActividad.setText(this.garante.getActivadaQueRealiza());
			this.txtComprobante.setText(this.garante.getComprobanteIngresos());
			this.checkGarante.setState(true);
			this.checkGarante.setEnabled(false);
		}

		this.txtDomicilio.setText(this.cliente.getDomicilio());
		this.txtEmail.setText(this.cliente.getEmail());
		this.txtEstadoCivil.setText(this.cliente.getEstadoCivil());
		this.txtTelefono.setText(this.cliente.getTelefono());
		this.txtTipoDoc.setText(this.cliente.getTipoDeDocumento());
		this.txtDocumento.setText(this.cliente.getDocumento());
		this.txtNombre.setText(this.cliente.getNombre());
		this.txtApellido.setText(this.cliente.getApellido());

		this.txtApellido.setEnabled(false);
		this.txtTipoDoc.setEnabled(false);
		this.txtCuit.setEnabled(false);
		this.txtDocumento.setEnabled(false);
		this.txtNombre.setEnabled(false);
		this.tipoCliente();

	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */

	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jLayeredPane1 = new javax.swing.JLayeredPane();
		panel1 = new java.awt.Panel();
		jLabel1 = new javax.swing.JLabel();
		jLabel2 = new javax.swing.JLabel();
		jLabel4 = new javax.swing.JLabel();
		jLabel5 = new javax.swing.JLabel();
		jLabel3 = new javax.swing.JLabel();
		jLabel10 = new javax.swing.JLabel();
		jLabel11 = new javax.swing.JLabel();
		jLabel12 = new javax.swing.JLabel();
		jLabel17 = new javax.swing.JLabel();
		txtNombre = new javax.swing.JTextField();
		txtApellido = new javax.swing.JTextField();
		txtTipoDoc = new javax.swing.JTextField();
		txtDocumento = new javax.swing.JTextField();
		txtDocumento.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {

				char caracter = e.getKeyChar();
				// Verificar si la tecla pulsada no es un digito
				if (((caracter < '0') || (caracter > '9')) && (caracter != '\b' /* BACK_SPACE */)) {
					e.consume(); // ignorar el evento de teclado
				}
			}
		});
		txtEstadoCivil = new javax.swing.JTextField();
		txtTelefono = new javax.swing.JTextField();
		txtDomicilio = new javax.swing.JTextField();
		txtEmail = new javax.swing.JTextField();
		checkbox1 = new java.awt.Checkbox();
		checkLocatario = new java.awt.Checkbox();
		checkbox3 = new java.awt.Checkbox();
		checkLocador = new java.awt.Checkbox();
		checkGarante = new java.awt.Checkbox();
		checkComprador = new java.awt.Checkbox();
		jPanel1 = new javax.swing.JPanel();
		jButton2 = new javax.swing.JButton();
		jButton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		btnGuardar = new javax.swing.JButton();
		lblCuit = new javax.swing.JLabel();
		lblRazon = new javax.swing.JLabel();
		txtCuit = new javax.swing.JTextField();
		txtRazonSocial = new javax.swing.JTextField();
		txtActividad = new javax.swing.JTextField();
		txtComprobante = new javax.swing.JTextField();
		lblUsuario = new javax.swing.JLabel();
		lblComprobante = new javax.swing.JLabel();
		txtContrasenia = new javax.swing.JTextField();
		lblActividadRealiza = new javax.swing.JLabel();
		lblContrasenia = new javax.swing.JLabel();
		txtUsuario = new javax.swing.JTextField();

		panel1.setBackground(new java.awt.Color(240, 240, 240));

		jLabel1.setText("Nombre");

		jLabel2.setText("Apellido");

		jLabel4.setText("Tipo doc");
		jLabel4.setToolTipText("");

		jLabel5.setText("Documento");

		jLabel3.setText("Tel�fono");

		jLabel10.setText("Domicilio");

		jLabel11.setText("Estado Civil");
		jLabel11.setToolTipText("");

		jLabel12.setText("E-mail");

		jLabel17.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
		jLabel17.setText("Datos de Cliente");

		txtDomicilio.setToolTipText("");

		checkbox1.setLabel("checkbox1");

		checkLocatario.setLabel("Locatario");
		checkLocatario.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(java.awt.event.ItemEvent evt) {
				checkLocatarioItemStateChanged(evt);
			}
		});

		checkbox3.setLabel("checkbox3");

		checkLocador.setLabel("Locador");
		checkLocador.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(java.awt.event.ItemEvent evt) {
				checkLocadorItemStateChanged(evt);
			}
		});

		checkGarante.setLabel("Garante");
		checkGarante.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(java.awt.event.ItemEvent evt) {
				checkGaranteItemStateChanged(evt);
			}
		});

		checkComprador.setLabel("Comprador");
		checkComprador.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(java.awt.event.ItemEvent evt) {
				checkCompradorItemStateChanged(evt);
			}
		});

		javax.swing.GroupLayout panel1Layout = new javax.swing.GroupLayout(panel1);
		panel1.setLayout(panel1Layout);
		panel1Layout.setHorizontalGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(panel1Layout.createSequentialGroup().addContainerGap().addGroup(panel1Layout
						.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(jLabel17)
						.addGroup(panel1Layout.createSequentialGroup().addGroup(panel1Layout
								.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addGroup(panel1Layout.createSequentialGroup().addComponent(jLabel5).addGap(18, 18, 18)
										.addComponent(txtDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 150,
												javax.swing.GroupLayout.PREFERRED_SIZE))
								.addGroup(panel1Layout.createSequentialGroup().addGroup(
										panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
												.addComponent(jLabel1).addComponent(jLabel2).addComponent(jLabel4))
										.addGap(34, 34, 34)
										.addGroup(panel1Layout
												.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
												.addComponent(txtTipoDoc, javax.swing.GroupLayout.PREFERRED_SIZE, 150,
														javax.swing.GroupLayout.PREFERRED_SIZE)
												.addComponent(txtApellido, javax.swing.GroupLayout.PREFERRED_SIZE, 150,
														javax.swing.GroupLayout.PREFERRED_SIZE)
												.addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 150,
														javax.swing.GroupLayout.PREFERRED_SIZE))))
								.addGap(76, 76, 76)
								.addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addComponent(jLabel11).addComponent(jLabel10).addComponent(jLabel3)
										.addComponent(jLabel12))
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
								.addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 150,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addComponent(txtTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 150,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addComponent(txtEstadoCivil, javax.swing.GroupLayout.PREFERRED_SIZE, 150,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addComponent(txtDomicilio, javax.swing.GroupLayout.PREFERRED_SIZE, 150,
												javax.swing.GroupLayout.PREFERRED_SIZE)))
						.addGroup(panel1Layout.createSequentialGroup().addGap(72, 72, 72)
								.addComponent(checkLocatario, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(checkLocador, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(checkGarante, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(checkComprador, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
						.addContainerGap(22, Short.MAX_VALUE)));
		panel1Layout.setVerticalGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
						.addContainerGap().addComponent(jLabel17)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
								.addGroup(panel1Layout.createSequentialGroup()
										.addGroup(panel1Layout
												.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel1)
												.addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGap(18, 18, 18)
										.addGroup(panel1Layout
												.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel2).addComponent(txtApellido,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGap(18, 18, 18)
										.addGroup(panel1Layout
												.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel4).addComponent(txtTipoDoc,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGap(18, 18, 18)
										.addGroup(panel1Layout
												.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel5).addComponent(txtDocumento,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE)))
								.addGroup(panel1Layout.createSequentialGroup()
										.addGroup(panel1Layout
												.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel11)
												.addComponent(txtEstadoCivil, javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
												javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addGroup(panel1Layout
												.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel10)
												.addComponent(txtDomicilio, javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGap(18, 18, 18)
										.addGroup(panel1Layout
												.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel3).addComponent(txtTelefono,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGap(18, 18, 18)
										.addGroup(panel1Layout
												.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE)
												.addComponent(jLabel12))))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
						.addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(checkLocatario, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(checkLocador, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(checkGarante, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(checkComprador, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addContainerGap()));

		jLayeredPane1.setLayer(panel1, javax.swing.JLayeredPane.DEFAULT_LAYER);

		javax.swing.GroupLayout jLayeredPane1Layout = new javax.swing.GroupLayout(jLayeredPane1);
		jLayeredPane1.setLayout(jLayeredPane1Layout);
		jLayeredPane1Layout.setHorizontalGroup(
				jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(panel1,
						javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
		jLayeredPane1Layout.setVerticalGroup(
				jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
						javax.swing.GroupLayout.Alignment.TRAILING,
						jLayeredPane1Layout.createSequentialGroup().addContainerGap()
								.addComponent(panel1, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		jButton2.setText("Cancelar");

		btnGuardar.setText("Guardar");
		btnGuardar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton1ActionPerformed(evt);
			}
		});

		lblCuit.setText("Cuit");
		lblCuit.setEnabled(false);

		lblRazon.setText("Razon Social");
		lblRazon.setEnabled(false);

		txtCuit.setEnabled(false);

		txtRazonSocial.setEnabled(false);

		txtActividad.setEnabled(false);

		txtComprobante.setToolTipText("");
		txtComprobante.setEnabled(false);

		lblUsuario.setText("Usuario Web");
		lblUsuario.setEnabled(false);

		lblComprobante.setText("Comprobante Ingreso");
		lblComprobante.setEnabled(false);

		txtContrasenia.setEnabled(false);

		lblActividadRealiza.setText("Actividad que Realiza");
		lblActividadRealiza.setToolTipText("");
		lblActividadRealiza.setEnabled(false);

		lblContrasenia.setText("Contrase�a");
		lblContrasenia.setEnabled(false);

		txtUsuario.setEnabled(false);

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
						jPanel1Layout.createSequentialGroup()
								.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 84,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
								.addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 84,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addContainerGap())
				.addGroup(jPanel1Layout.createSequentialGroup().addContainerGap()
						.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(lblCuit).addComponent(lblRazon))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(txtRazonSocial, javax.swing.GroupLayout.PREFERRED_SIZE, 150,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(txtCuit, javax.swing.GroupLayout.PREFERRED_SIZE, 150,
										javax.swing.GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
								javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(lblActividadRealiza).addComponent(lblComprobante).addComponent(lblUsuario)
								.addComponent(lblContrasenia))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(txtContrasenia, javax.swing.GroupLayout.PREFERRED_SIZE, 150,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 150,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(txtActividad, javax.swing.GroupLayout.PREFERRED_SIZE, 150,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(txtComprobante, javax.swing.GroupLayout.PREFERRED_SIZE, 150,
										javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGap(21, 21, 21)));
		jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
						.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(lblActividadRealiza).addComponent(
										txtActividad, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 21, Short.MAX_VALUE)
						.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(lblComprobante)
								.addComponent(txtComprobante, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGap(18, 18, 18)
						.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(lblUsuario)
								.addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGap(18, 18, 18)
						.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(txtContrasenia, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(lblContrasenia))
						.addGap(69, 69, 69)
						.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 41,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 41,
										javax.swing.GroupLayout.PREFERRED_SIZE))
						.addContainerGap())
				.addGroup(jPanel1Layout.createSequentialGroup().addContainerGap()
						.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(lblCuit).addComponent(txtCuit, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGap(18, 18, 18)
						.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(lblRazon).addComponent(txtRazonSocial,
										javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.PREFERRED_SIZE))
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addComponent(jLayeredPane1).addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE,
						javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addComponent(jLayeredPane1, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jPanel1,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
								Short.MAX_VALUE)));

		pack();
	}// </editor-fold>

	private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
		this.botonGuardar();
	}

	private void checkLocatarioItemStateChanged(java.awt.event.ItemEvent evt) {
		this.tipoCliente();
	}

	private void checkLocadorItemStateChanged(java.awt.event.ItemEvent evt) {
		this.tipoCliente();
	}

	private void checkGaranteItemStateChanged(java.awt.event.ItemEvent evt) {
		this.tipoCliente();
	}

	private void checkCompradorItemStateChanged(java.awt.event.ItemEvent evt) {
		this.tipoCliente();
	}

	private void enableCompradorLocatario(boolean estado) {
		this.lblActividadRealiza.setEnabled(estado);
		this.txtComprobante.setEnabled(estado);
		this.lblComprobante.setEnabled(estado);
		this.txtActividad.setEnabled(estado);
		this.lblUsuario.setEnabled(estado);
		this.txtUsuario.setEnabled(estado);
		this.lblContrasenia.setEnabled(estado);
		this.txtContrasenia.setEnabled(estado);
	}

	private void tipoCliente() {

		enableCompradorLocatario(false);
		this.txtCuit.setEnabled(false);
		this.txtRazonSocial.setEnabled(false);
		this.lblCuit.setEnabled(false);
		this.lblRazon.setEnabled(false);

		if (checkLocatario.getState()) {
			enableCompradorLocatario(true);
		}
		if (checkLocador.getState()) {
			this.lblCuit.setEnabled(true);
			this.txtCuit.setEnabled(true);
			this.lblRazon.setEnabled(true);
			this.txtRazonSocial.setEnabled(true);
		}
		if (checkComprador.getState()) {
			enableCompradorLocatario(true);
		}
		if (checkGarante.getState()) {
			this.lblActividadRealiza.setEnabled(true);
			this.txtComprobante.setEnabled(true);
			this.lblComprobante.setEnabled(true);
			this.txtActividad.setEnabled(true);
		}
	}

	private void botonGuardar() {
		if (this.cliente == null) {
			guardar();
		} else {
			actualizar();
		}
	}

	private void guardar() {

		try {
			this.NuevoCliente = new Cliente(this.txtTipoDoc.getText(), this.txtDocumento.getText(),
					this.txtNombre.getText(), this.txtApellido.getText(), this.txtEstadoCivil.getText(),
					this.txtDomicilio.getText(), this.txtTelefono.getText(), this.txtEmail.getText());
			this.controlador.agregarCliente(this.NuevoCliente);

			if (this.checkLocador.getState()) {
				this.locador = new Locador(this.NuevoCliente, this.txtCuit.getText(), this.txtRazonSocial.getText());
				this.controlador.agregarTipoCliente(this.locador);
			}
			if (this.checkLocatario.getState()) {
				this.locatario = new Locatario(this.NuevoCliente, this.txtActividad.getText(),
						this.txtComprobante.getText(), this.txtUsuario.getText(), this.txtContrasenia.getText());
				this.controlador.agregarTipoCliente(this.locatario);
			}
			if (this.checkComprador.getState()) {
				this.comprador = new Comprador(this.NuevoCliente, this.txtActividad.getText(),
						this.txtComprobante.getText(), this.txtUsuario.getText(), this.txtContrasenia.getText());
				this.controlador.agregarTipoCliente(this.comprador);
			}
			if (this.checkGarante.getState()) {
				this.garante = new Garante(this.NuevoCliente, this.txtActividad.getText(),
						this.txtComprobante.getText());
				this.controlador.agregarTipoCliente(this.garante);
			}

			this.setVisible(false);
		} catch (BadPostCodeException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}

	}

	private void actualizar() {

		this.cliente.setDomicilio(this.txtDomicilio.getText());
		this.cliente.setTelefono(this.txtTelefono.getText());
		this.cliente.setEmail(this.txtEmail.getText());
		this.cliente.setEstadoCivil(this.txtEstadoCivil.getText());
		this.controlador.actualizarCliente(this.cliente);
		try {

			if (this.checkLocador.getState()) {
				if (this.locador != null) {

					this.locador.setRazonSocial(this.txtRazonSocial.getText());
					this.controlador.actualizarCliente(this.locador);
				} else {
					this.locador = new Locador(this.cliente, this.txtCuit.getText(), this.txtRazonSocial.getText());
					this.controlador.agregarTipoCliente(this.locador);
				}
			}
			if (this.checkComprador.getState()) {
				if (this.comprador != null) {
					this.comprador.setActivadaQueRealiza(this.txtActividad.getText());
					this.comprador.setComprobanteIngresos(this.txtComprobante.getText());
					this.comprador.setUsuarioWeb(this.txtUsuario.getText());
					this.comprador.setContraseniaWeb(this.txtContrasenia.getText());
					this.controlador.actualizarCliente(this.comprador);
				} else {
					this.comprador = new Comprador(this.cliente, this.txtActividad.getText(),
							this.txtComprobante.getText(), this.txtUsuario.getText(), this.txtContrasenia.getText());
					this.controlador.agregarTipoCliente(this.comprador);
				}
			}
			if (this.checkLocatario.getState()) {
				if (this.locatario != null) {
					this.locatario.setActividadQueRealiza(this.txtActividad.getText());
					this.locatario.setComprobanteIngreso(this.txtComprobante.getText());
					this.locatario.setUsuarioWeb(this.txtUsuario.getText());
					this.locatario.setContrasenia(this.txtContrasenia.getText());
					this.controlador.actualizarCliente(this.locatario);

				} else {
					this.locatario = new Locatario(this.cliente, this.txtActividad.getText(),
							this.txtComprobante.getText(), this.txtUsuario.getText(), this.txtContrasenia.getText());
					this.controlador.agregarTipoCliente(this.locatario);
				}
			}
			if (this.checkGarante.getState()) {
				if (this.garante != null) {
					this.garante.setActivadaQueRealiza(this.txtActividad.getText());
					this.garante.setComprobanteIngresos(this.txtComprobante.getText());
					this.controlador.actualizarCliente(this.garante);
				} else {
					this.garante = new Garante(this.cliente, this.txtActividad.getText(),
							this.txtComprobante.getText());
					this.controlador.agregarTipoCliente(this.garante);
				}
			}
		} catch (BadPostCodeException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		this.setVisible(false);
	}
}
