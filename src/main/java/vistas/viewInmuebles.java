
package vistas;

import java.awt.Frame;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.table.DefaultTableModel;

import controlador.Controlador;
import excepcion.BadPostCodeException;
import modelo.Casa;
import modelo.Departamento;
import modelo.Inmueble;
import modelo.LocalComercial;
import modelo.Terreno;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JMenu;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.SwingConstants;

/**
 *
 * @author Juan Quiros
 */
public class viewInmuebles extends  JInternalFrame {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Creates new form viewInmuebles
	 */
	private javax.swing.JButton jButton1;
	private javax.swing.JButton jButton2;
	private javax.swing.JButton jButton3;
	private javax.swing.JComboBox<String> jComboBox1;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel10;
	private javax.swing.JLabel jLabel11;
	private javax.swing.JLabel jLabel12;
	private javax.swing.JLabel jLabel17;
	private javax.swing.JLabel jLabel18;
	private javax.swing.JLabel jLabel19;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel20;
	private javax.swing.JLabel jLabel21;
	private javax.swing.JLabel jLabel22;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JLabel jLabel5;
	private javax.swing.JLayeredPane jLayeredPane1;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JPanel jPanel2;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JSpinner jSpinner1;
	private javax.swing.JSpinner jSpinner2;
	private javax.swing.JTable jTable1;
	private javax.swing.JLabel lblApellido;
	private javax.swing.JLabel lblDocumento;
	private javax.swing.JLabel lblDomicilio;
	private javax.swing.JLabel lblEmail;
	private javax.swing.JLabel lblEstadoCivil;
	private javax.swing.JLabel lblNombre;
	private javax.swing.JLabel lblTelefono;
	private javax.swing.JLabel lblTipoDoc;
	private java.awt.Panel panel1;
	private Controlador controlador;
	private ArrayList<Inmueble> datosDeTabla;
	private Frame parent;
	private JMenuBar menuBar;
	private JMenu mnNewMenu;
	private JMenu mnNewMenu_1;
	private JMenu mnNewMenu_2;
	private JMenu mnVerDetalles;
	
    public viewInmuebles(Frame parent,Controlador controlador) {
    	setTitle("Inmuebles");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 664, 459);
		((javax.swing.plaf.basic.BasicInternalFrameUI)this.getUI()).setNorthPane(null);
		initComponents();
		this.controlador = controlador;
		this.parent= parent;
		
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		mnNewMenu = new JMenu("Nuevo");
		mnNewMenu.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				
				
				nuevoInmueble();
				
			}
		});
		menuBar.add(mnNewMenu);
		
		mnNewMenu_1 = new JMenu("Editar");
		mnNewMenu_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
			jMenu1MenuKeyPressed();
			}
		});
		menuBar.add(mnNewMenu_1);
		
		mnNewMenu_2 = new JMenu("Borrar");
		mnNewMenu_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				jMenu2MenuKeyPressed();
			}
		});
		menuBar.add(mnNewMenu_2);
		
		mnVerDetalles = new JMenu("Ver Detalles");
		mnVerDetalles.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				verDetalles();
			}
		});
		menuBar.add(mnVerDetalles);
		datosDeTabla = this.controlador.obtenerTodosLosInmuebles();
		cargarTabla(datosDeTabla);
    }
                          
    private void initComponents() {

        jLayeredPane1 = new javax.swing.JLayeredPane();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jTable1.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mousePressed(MouseEvent e) {
        		cargarDatosLocador();
        	}
        });
        jTable1.addKeyListener(new KeyAdapter() {
        	@Override
        	public void keyPressed(KeyEvent arg0) {
        		cargarDatosLocador();
        	}
        });
        
        jLabel21 = new javax.swing.JLabel();
        panel1 = new java.awt.Panel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel4.setHorizontalAlignment(SwingConstants.LEFT);
        jLabel5 = new javax.swing.JLabel();
        lblNombre = new javax.swing.JLabel();
        lblNombre.setHorizontalAlignment(SwingConstants.LEFT);
        lblApellido = new javax.swing.JLabel();
        lblApellido.setHorizontalAlignment(SwingConstants.LEFT);
        lblTipoDoc = new javax.swing.JLabel();
        lblTipoDoc.setHorizontalAlignment(SwingConstants.LEFT);
        lblDocumento = new javax.swing.JLabel();
        lblDocumento.setHorizontalAlignment(SwingConstants.LEFT);
        jLabel3 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        lblTelefono = new javax.swing.JLabel();
        lblTelefono.setHorizontalAlignment(SwingConstants.LEFT);
        lblDomicilio = new javax.swing.JLabel();
        lblDomicilio.setHorizontalAlignment(SwingConstants.LEFT);
        lblEstadoCivil = new javax.swing.JLabel();
        lblEstadoCivil.setHorizontalAlignment(SwingConstants.LEFT);
        lblEmail = new javax.swing.JLabel();
        lblEmail.setHorizontalAlignment(SwingConstants.LEFT);
        jLabel17 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jSpinner1 = new javax.swing.JSpinner();
        jLabel20 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jSpinner2 = new javax.swing.JSpinner();
        jLabel22 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jButton3.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mousePressed(MouseEvent arg0) {
        		generarContrato(true);
        	}
        });
        jButton2 = new javax.swing.JButton();
        jButton2.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mousePressed(MouseEvent e) {
        		generarContrato(false);
        	}
        });
        jButton1 = new javax.swing.JButton();
        jButton1.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mousePressed(MouseEvent arg0) {
        		buscarInmueble();
        	}
        });

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Tipo de Inmueble", "Ubicacion", "m2", "Estado"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel21.setText("Inmuebles Registrados");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel21)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel21)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel1.setText("Nombre");

        jLabel2.setText("Apellido");

        jLabel4.setText("Tipo doc");
        jLabel4.setToolTipText("");

        jLabel5.setText("Documento");

        lblNombre.setText("");

        lblApellido.setText("");

        lblTipoDoc.setText("");

        lblDocumento.setText("");

        jLabel3.setText("Tel�fono");

        jLabel10.setText("Domicilio");

        jLabel11.setText("Estado Civil");
        jLabel11.setToolTipText("");

        jLabel12.setText("E-mail");

        lblTelefono.setText("");

        lblDomicilio.setText("");

        lblEstadoCivil.setText("");

        lblEmail.setText("");

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel17.setText("Datos de Locador del Inmueble Seleccionado");

        javax.swing.GroupLayout panel1Layout = new javax.swing.GroupLayout(panel1);
        panel1.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel17)
                    .addGroup(panel1Layout.createSequentialGroup()
                        .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(panel1Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblNombre))
                            .addGroup(panel1Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblApellido))
                            .addGroup(panel1Layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblTipoDoc))
                            .addGroup(panel1Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addGap(18, 18, 18)
                                .addComponent(lblDocumento)))
                        .addGap(194, 194, 194)
                        .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(panel1Layout.createSequentialGroup()
                                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel10)
                                    .addComponent(jLabel11)
                                    .addComponent(jLabel12))
                                .addGap(18, 18, 18)
                                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(lblDomicilio)
                                    .addComponent(lblEstadoCivil)
                                    .addComponent(lblEmail, javax.swing.GroupLayout.Alignment.TRAILING)))
                            .addGroup(panel1Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblTelefono)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel17)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel1Layout.createSequentialGroup()
                        .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(lblNombre))
                        .addGap(18, 18, 18)
                        .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(lblApellido))
                        .addGap(18, 18, 18)
                        .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(lblTipoDoc))
                        .addGap(18, 18, 18)
                        .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(lblDocumento)))
                    .addGroup(panel1Layout.createSequentialGroup()
                        .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblEstadoCivil)
                            .addComponent(jLabel11))
                        .addGap(18, 18, 18)
                        .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblDomicilio)
                            .addComponent(jLabel10))
                        .addGap(18, 18, 18)
                        .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(lblTelefono))
                        .addGap(18, 18, 18)
                        .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(lblEmail))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel18.setText("Buscar Inmueble");

        jLabel19.setText("Metros Cuadrados");

        jLabel20.setText("Tipo Inmueble");

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Casa", "Departamento", "Local Comercial", "Terreno" }));

        jLabel22.setText("Hasta");

        jButton3.setText("Alquilar Inmueble");

        jButton2.setText("Vender Inmueble");

        jButton1.setText("Buscar");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel19)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel22)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jSpinner2, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel20))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel18)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jButton3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(jButton3)
                    .addComponent(jButton2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(jSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel20)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSpinner2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel22)
                    .addComponent(jButton1))
                .addContainerGap())
        );

        jLayeredPane1.setLayer(jPanel2, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(panel1, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(jPanel1, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLayeredPane1Layout = new javax.swing.GroupLayout(jLayeredPane1);
        jLayeredPane1.setLayout(jLayeredPane1Layout);
        jLayeredPane1Layout.setHorizontalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jLayeredPane1Layout.setVerticalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLayeredPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLayeredPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 2, Short.MAX_VALUE))
        );

        pack();
    }
    private void generarContrato(boolean alquila) {
    	int indice = jTable1.getSelectedRow();
		if (indice > -1) {
			try {
	    		new viewNuevoContrato(this.parent, true, this.controlador ,  this.datosDeTabla.get(indice),alquila).setVisible(true);
	    	}catch(BadPostCodeException e) {
	    		JOptionPane.showConfirmDialog(null, e.getMessage(),"Error",2);
	    	}
		}else {
			JOptionPane.showMessageDialog(null, "Debe seleccionar un Inmueble","Alerta",1);
		}
    
    	
    }
    private void cargarTabla(ArrayList<Inmueble> Inmuebles) {
		Vector<Object> fila;
		DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
		for (int i = dtm.getRowCount(); i > 0; i--) {
			dtm.removeRow(i - 1);
		}
		if (Inmuebles != null) {
			for (Inmueble unInmueble : Inmuebles) {
				fila = new Vector<Object>();
				if (unInmueble instanceof Casa) {
					fila.add("Casa");
				}
				if (unInmueble instanceof Departamento) {
					fila.add("Departamento");
				}
				if (unInmueble instanceof LocalComercial) {
					fila.add("Local Com.");
				}
				if (unInmueble instanceof Terreno) {
					fila.add("Terreno");
				}
				fila.add(unInmueble.getUbicacion());
				fila.add(unInmueble.getMetrosCuadrados());
				if(unInmueble.isHabilidato()){
					fila.add("Si");
				}else{
					fila.add("NO");
				}				
				dtm.addRow(fila);
			}
		}
	}
    private void nuevoInmueble(){
    	viewNuevoInmueble viewNuevoinmueble;
		viewNuevoinmueble = new viewNuevoInmueble(this.parent, true, this.controlador,null);
		viewNuevoinmueble.setVisible(true);
		datosDeTabla = this.controlador.obtenerTodosLosInmuebles();
		cargarTabla(datosDeTabla);
    }
    private void buscarInmueble(){
    	
    	this.datosDeTabla = this.controlador.obtenerInmueblePorM2yTipo((int)this.jSpinner1.getValue(),(int)this.jSpinner2.getValue(),jComboBox1.getSelectedItem().toString());
    	this.cargarTabla(this.datosDeTabla);
    }       
    private void verDetalles(){
    	int indice = jTable1.getSelectedRow();
		if (indice > -1) {
			JOptionPane.showMessageDialog(null, this.datosDeTabla.get(indice).toString(), "Error", 2);
		} else {
			JOptionPane.showMessageDialog(null, "Debe seleccionar un Inmueble para ver todos los detalles","Alerta",1);
		}
    }
    private void jMenu2MenuKeyPressed() {
		int indice = jTable1.getSelectedRow();
		if(indice > -1 ) {
			if(this.datosDeTabla.get(indice).isHabilidato()) {
			if (JOptionPane.showConfirmDialog(null, "Seguro quiere eliminar al Inmueble seleccionado? Se borraran los contratos finalizados",
					"Borrar", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				
				this.controlador.borrarInmueble(this.datosDeTabla.get(indice));
				datosDeTabla = this.controlador.obtenerTodosLosInmuebles();
				cargarTabla(datosDeTabla);
				
			}
			datosDeTabla = this.controlador.obtenerTodosLosInmuebles();
			cargarTabla(datosDeTabla);
			}else {
				JOptionPane.showMessageDialog(null, "El inmueble contiene contratos vigentes","Alerta",1);
			}
		} else {
			JOptionPane.showMessageDialog(null, "Debe seleccionar un Inmueble para borrar","Alerta",1);
		}
	}
    private void jMenu1MenuKeyPressed() {
		int indice = jTable1.getSelectedRow();
		viewNuevoInmueble viewNuevoinmueble;
		if (indice > -1) {			
			viewNuevoinmueble = new viewNuevoInmueble(this.parent, true, this.controlador,this.datosDeTabla.get(indice));
			viewNuevoinmueble.setVisible(true);			
		} else {
			JOptionPane.showMessageDialog(null, "Debe seleccionar un Inmueble para editar","Alerta",1);
		}
		datosDeTabla = this.controlador.obtenerTodosLosInmuebles();
		cargarTabla(datosDeTabla);
	}
    private void cargarDatosLocador(){
    	int indice = jTable1.getSelectedRow();		
    	  if (indice > -1) {	
    		  this.lblNombre.setText(this.datosDeTabla.get(indice).getUnLocador().getCliente().getNombre());
    		  this.lblApellido.setText(this.datosDeTabla.get(indice).getUnLocador().getCliente().getApellido());
    		  this.lblTipoDoc.setText(this.datosDeTabla.get(indice).getUnLocador().getCliente().getTipoDeDocumento());
    		  this.lblDocumento.setText(this.datosDeTabla.get(indice).getUnLocador().getCliente().getDocumento());
    		  this.lblDomicilio.setText(this.datosDeTabla.get(indice).getUnLocador().getCliente().getDomicilio());
    		  this.lblEstadoCivil.setText(this.datosDeTabla.get(indice).getUnLocador().getCliente().getEstadoCivil());
    		  this.lblTelefono.setText(this.datosDeTabla.get(indice).getUnLocador().getCliente().getTelefono());
    		  this.lblEmail.setText(this.datosDeTabla.get(indice).getUnLocador().getCliente().getEmail());						
		} else {
			this.lblNombre.setText("");
			this.lblApellido.setText("");
			this.lblTipoDoc.setText("");
			this.lblDocumento.setText("");
			this.lblDomicilio.setText("");
			this.lblEstadoCivil.setText("");
			this.lblTelefono.setText("");
			this.lblEmail.setText("");
		}
    }
}
