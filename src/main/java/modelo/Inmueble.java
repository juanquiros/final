package modelo;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name ="Inmueble")
@Inheritance(strategy = InheritanceType.JOINED)
public class Inmueble implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="cod_inmueble", unique=true, updatable=false,nullable =false)
	private int cod_inmueble;
	

	@Column(name="ubicacion")
    private String ubicacion;
	
		
	@Column(name="metrosCuadrados")
    private int metrosCuadrados;
	
	
	@Column(name="habilidato")
    private boolean habilidato;
	
	
	
	 @ManyToOne
	@JoinColumn(name="locador_cod_locador") 
    private Locador locador;
	
	
	public Inmueble(){}
	public Inmueble(String ubicacion, int metrosCuadrados, boolean habilidato, Locador locador) {
		this.ubicacion = ubicacion;
		this.metrosCuadrados = metrosCuadrados;
		this.habilidato = habilidato;
		this.locador = locador;
	}
	public int getCod_inmueble() {
		return cod_inmueble;
	}
	public void setCod_inmueble(int cod_inmueble) {
		this.cod_inmueble = cod_inmueble;
	}
	public String getUbicacion() {
		return ubicacion;
	}
	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}
	public int getMetrosCuadrados() {
		return metrosCuadrados;
	}
	public void setMetrosCuadrados(int metrosCuadrados) {
		this.metrosCuadrados = metrosCuadrados;
	}
	public boolean isHabilidato() {
		return habilidato;
	}
	public void setHabilidato(boolean habilidato) {
		this.habilidato = habilidato;
	}
	public Locador getUnLocador() {
		return locador;
	}
	public void setUnLocador(Locador locador) {
		this.locador = locador;
	}
	public String descripcionInmueble(){
		return "Datos de Inmueble:" + ubicacion +", "+ metrosCuadrados +", "+ habilidato +", \n"+ locador.toString();
	}
}
