package modelo;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@PrimaryKeyJoinColumn(name="fk_inmueble")
@Table(name="Departamentos")
public class Departamento extends Inmueble implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@OneToMany(mappedBy="departamentoAlquilado",cascade=CascadeType.REMOVE)
	private Set<Alquiler> alquiler;
	public Departamento(){}
	public Departamento(String ubicacion, int metrosCuadrados, boolean habilidato, Locador unLocador){
		super(ubicacion,metrosCuadrados,habilidato,unLocador);
	}
	
	
	public String toString(){
		return this.descripcionInmueble() + " Tipo: Departamento ";
	}
}
