package modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity(name="fk_contrato_comicionInmo")
@Table(name ="ComicionInmobiliaria")
public class ComicionInmobiliaria implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="cod_ComInmob", unique=true, updatable=false,nullable =false)
	private int cod_ComInmob;
	
	
	@Column(name="montoComicion")
    private float montoComicion;
	
	@Column(name="cantidadDePagos")
    private int cantidadDePagos;
	
	@OneToOne(mappedBy="comicionInmobiliari")
	private Contrato contrato;
	
	public ComicionInmobiliaria(){}
	public ComicionInmobiliaria(float montoComicion, int cantidadDePagos) {
		this.montoComicion = montoComicion;
		this.cantidadDePagos = cantidadDePagos;
	}
	public int getCod_ComInmob() {
		return cod_ComInmob;
	}
	public void setCod_ComInmob(int cod_ComInmob) {
		this.cod_ComInmob = cod_ComInmob;
	}
	public float getMontoComicion() {
		return montoComicion;
	}
	public void setMontoComicion(float montoComicion) {
		this.montoComicion = montoComicion;
	}
	public int getCantidadDePagos() {
		return cantidadDePagos;
	}
	public void setCantidadDePagos(int cantidadDePagos) {
		this.cantidadDePagos = cantidadDePagos;
	}
	public String toString(){
		return "Comicion Inmobiliaria: " + "$" + montoComicion + ", en " + cantidadDePagos + " pagos.";
	}
}
