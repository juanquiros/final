package modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name ="ArancelEspecial")
public class ArancelEspecial implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="cod_ArancelEspecial", unique=true, updatable=false,nullable =false)
	private int cod_ArancelEspecial;
	
	
	@Column(name="tipoArancel")
    private String tipoArancel;
	
	@Column(name="monto")
    private float monto;
	
	@Column(name="habilitado")
    private boolean habilitado;
	
	@ManyToOne
	private Contrato contrato;
	
	public ArancelEspecial(){}
	public ArancelEspecial(String tipoArancel, float monto, boolean habilitado, Contrato contrato) {
		this.tipoArancel = tipoArancel;
		this.monto = monto;
		this.habilitado = habilitado;
		 this.contrato = contrato;
	}
	
	
	
	public Contrato getContrato() {
		return contrato;
	}
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	public int getCod_ArancelEspecial() {
		return cod_ArancelEspecial;
	}
	public void setCod_ArancelEspecial(int cod_ArancelEspecial) {
		this.cod_ArancelEspecial = cod_ArancelEspecial;
	}
	public String getTipoArancel() {
		return tipoArancel;
	}
	public void setTipoArancel(String tipoArancel) {
		this.tipoArancel = tipoArancel;
	}
	public float getMonto() {
		return monto;
	}
	public void setMonto(float monto) {
		this.monto = monto;
	}
	public boolean isHabilitado() {
		return habilitado;
	}
	public void setHabilitado(boolean habilitado) {
		this.habilitado = habilitado;
	}
	public String toString(){
		return "Arancel: " + tipoArancel +" monto $"+ monto +" ->" + habilitado;
	}
}
