package modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name ="Recargo")
public class Recargo implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="cod_recargo", unique=true, updatable=false,nullable =false)
	private int cod_recargo;
	
	@Column(name="montoFijo")
    private float montoFijo;
	
	@Column(name="montoProporcional")
    private float montoProporcional;
	
	@Column(name="habilitado")
    private boolean habilitado;
	
	@OneToOne(mappedBy="recargo")
	private Contrato contrato;
	
	public Recargo(){}
	public Recargo(float montoFijo, float montoProporcional, boolean habilitado) {
		this.montoFijo = montoFijo;
		this.montoProporcional = montoProporcional;
		this.habilitado = habilitado;
	}
	public int getCod_recargo() {
		return cod_recargo;
	}
	public void setCod_recargo(int cod_recargo) {
		this.cod_recargo = cod_recargo;
	}
	public float getMontoFijo() {
		return montoFijo;
	}
	public void setMontoFijo(float montoFijo) {
		this.montoFijo = montoFijo;
	}
	public float getMontoProporcional() {
		return montoProporcional;
	}
	public void setMontoProporcional(float montoProporcional) {
		this.montoProporcional = montoProporcional;
	}
	public boolean isHabilitado() {
		return habilitado;
	}
	public void setHabilitado(boolean habilitado) {
		this.habilitado = habilitado;
	}
	public String toString(){
		return "Recargo: " + "$" + montoFijo+ " " + montoProporcional+ "% ->" + habilitado;
	}
}
